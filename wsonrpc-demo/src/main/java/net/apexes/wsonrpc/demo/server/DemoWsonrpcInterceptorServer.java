package net.apexes.wsonrpc.demo.server;

import net.apexes.wsonrpc.server.WsonrpcServer;

/**
 * @author hedyn
 */
public class DemoWsonrpcInterceptorServer extends DemoWsonrpcServer {

    public static void main(String[] args) throws Exception {
        new DemoWsonrpcServer().launchVertxServer();
    }

    @Override
    protected void bindService(WsonrpcServer serverBase) {
        serverBase.setWsonrpcRequestInterceptor(new DemoWsonrpcInterceptorImpl());
    }
}
