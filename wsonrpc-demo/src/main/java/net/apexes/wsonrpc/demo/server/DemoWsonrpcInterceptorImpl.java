package net.apexes.wsonrpc.demo.server;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import net.apexes.wsonrpc.demo.api.model.Dept;
import net.apexes.wsonrpc.demo.api.model.Role;
import net.apexes.wsonrpc.demo.api.model.User;
import net.apexes.wsonrpc.demo.server.service.DemoServiceImpl;
import net.apexes.wsonrpc.demo.server.service.RegisterServiceImpl;
import net.apexes.wsonrpc.json.JsonRpcRequest;
import net.apexes.wsonrpc.server.WsonrpcRequestInterceptor;

import java.lang.reflect.Type;
import java.util.List;

/**
 * @author hedyn
 */
public class DemoWsonrpcInterceptorImpl implements WsonrpcRequestInterceptor {

    private final RegisterServiceImpl registerService = new RegisterServiceImpl();
    private final DemoServiceImpl demoService = new DemoServiceImpl();

    private final Gson gson = new Gson();

    @Override
    public void handle(Context context) {
        JsonRpcRequest request = context.getRequest();
        // 模拟根据 method 调用其他服务
        switch (request.getMethod()) {
            case "RegisterService.register":
                onRegister(context, request);
                break;
            case "RegisterService.login":
                onLogin(context, request);
                break;
            case "DemoService.echo":
                onEcho(context, request);
                break;
            case "DemoService.login":
                onDemoLogin(context, request);
                break;
            case "DemoService.getRoleList":
                onGetRoleList(context, request);
                break;
            case "DemoService.getDept":
                onGetDept(context, request);
                break;
            case "DemoService.getDeptList":
                onGetDeptList(context, request);
                break;
            case "DemoService.listUser":
                onListUsern(context, request);
                break;
            case "DemoService.addUser":
                onAddUser(context, request);
                break;
            case "DemoService.addRole":
                onAddRole(context, request);
                break;
            case "DemoService.addDept":
                onAddDept(context, request);
                break;
        }
    }

    private void onRegister(Context context, JsonRpcRequest request) {
        String clientId = gson.fromJson(request.getParamAsJson(0), String.class);
        boolean res = registerService.register(clientId);
        context.replyValue(res);
    }

    private void onLogin(Context context, JsonRpcRequest request) {
        String username = gson.fromJson(request.getParamAsJson(0), String.class);
        String password = gson.fromJson(request.getParamAsJson(1), String.class);;
        User user = registerService.login(username, password);
        context.replyValue(user);
    }

    private void onEcho(Context context, JsonRpcRequest request) {
        String text = gson.fromJson(request.getParamAsJson(0), String.class);;
        String res = demoService.echo(text);
        context.replyValue(res);
    }

    private void onDemoLogin(Context context, JsonRpcRequest request) {
        String username = gson.fromJson(request.getParamAsJson(0), String.class);
        String password = gson.fromJson(request.getParamAsJson(1), String.class);;
        User user = demoService.login(username, password);
        context.replyValue(user);
    }

    private void onGetRoleList(Context context, JsonRpcRequest request) {
        List<Role> list = demoService.getRoleList();
        context.replyValue(list);
    }

    private void onGetDept(Context context, JsonRpcRequest request) {
        String name = gson.fromJson(request.getParamAsJson(0), String.class);
        Dept res = demoService.getDept(name);
        context.replyValue(res);
    }

    private void onGetDeptList(Context context, JsonRpcRequest request) {
        List<Dept> list = demoService.getDeptList();
        context.replyValue(list);
    }

    private void onListUsern(Context context, JsonRpcRequest request) {
        Type type = TypeToken.getParameterized(List.class, String.class).getType();
        List<String> usernames = gson.fromJson(request.getParamAsJson(0), type);
        List<User> list = demoService.listUser(usernames);
        context.replyValue(list);
    }

    private void onAddUser(Context context, JsonRpcRequest request) {
        User user = gson.fromJson(request.getParamAsJson(0), User.class);
        User res = demoService.addUser(user);
        context.replyValue(res);
    }

    private void onAddRole(Context context, JsonRpcRequest request) {
        Role role = gson.fromJson(request.getParamAsJson(0), Role.class);
        demoService.addRole(role);
    }

    private void onAddDept(Context context, JsonRpcRequest request) {
        Dept dept = gson.fromJson(request.getParamAsJson(0), Dept.class);
        boolean res = demoService.addDept(dept);
        context.replyValue(res);
    }
}
