package net.apexes.wsonrpc.demo.util;

import net.apexes.wsonrpc.core.JsonRpcLogger;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author hedyn
 */
public class SimpleJsonRpcLogger implements JsonRpcLogger {

    public static final SimpleJsonRpcLogger DEFAULT = new SimpleJsonRpcLogger();

    private static final Logger LOG = LoggerFactory.getLogger(SimpleJsonRpcLogger.class);

    @Override
    public void onReceive(String from, String json) {
        LOG.info("<< [{}] {}", from, json);
    }

    @Override
    public void onTransmit(String dest, String json) {
        LOG.info(">> [{}] {}", dest, json);
    }
}
