/*
 * Copyright (C) 2016, apexes.net. All rights reserved.
 * 
 *        http://www.apexes.net
 * 
 */
package net.apexes.wsonrpc.demo.client.service;

import net.apexes.wsonrpc.demo.api.DemoService;
import net.apexes.wsonrpc.demo.api.PushService;
import net.apexes.wsonrpc.demo.client.WsonrpcClientWarpper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * 
 * @author <a href="mailto:hedyn@foxmail.com">HeDYn</a>
 *
 */
public class PushServiceImpl implements PushService {
    
    private static final Logger LOG = LoggerFactory.getLogger(PushServiceImpl.class);

    private final WsonrpcClientWarpper client;

    public PushServiceImpl(WsonrpcClientWarpper client) {
        this.client = client;
    }

    @Override
    public String setupStatus(String value) {
        LOG.info("setupStatus: {}", value);
        return "clientStatus : {" + value + "}";
    }

    @Override
    public void notice(String message) {
        LOG.info("onNotice: {}", message);
        client.wsonrpcService(DemoService.class).echo(message);
    }

}
