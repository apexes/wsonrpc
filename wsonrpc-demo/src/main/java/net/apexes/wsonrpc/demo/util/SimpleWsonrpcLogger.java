/*
 * Copyright (c) 2018, apexes.net. All rights reserved.
 *
 *         http://www.apexes.net
 *
 */
package net.apexes.wsonrpc.demo.util;

import net.apexes.wsonrpc.client.WsonrpcClientLogger;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.net.URI;

/**
 * @author <a href=mailto:hedyn@foxmail.com>HeDYn</a>
 */
public class SimpleWsonrpcLogger implements WsonrpcClientLogger {

    public static final SimpleWsonrpcLogger DEFAULT = new SimpleWsonrpcLogger();

    private static final Logger LOG = LoggerFactory.getLogger(SimpleWsonrpcLogger.class);

    @Override
    public void onReceive(String sessionId, String json) {
        LOG.info("<< [{}] {}", sessionId, json);
    }

    @Override
    public void onTransmit(String sessionId, String json) {
        LOG.info(">> [{}] {}", sessionId, json);
    }

    @Override
    public void onError(String sessionId, Throwable error) {
        LOG.warn("xx [" + sessionId + "]", error);
    }

    @Override
    public void onConnectError(URI uri, Throwable error) {
        LOG.error("connect error : " + uri, error);
    }

    @Override
    public void onPingError(Throwable error) {
        LOG.error("ping error", error);
    }
}
