/*
 * Copyright (C) 2016, apexes.net. All rights reserved.
 * 
 *        http://www.apexes.net
 * 
 */
package net.apexes.wsonrpc.demo.server;

import net.apexes.wsonrpc.demo.api.DemoService;
import net.apexes.wsonrpc.demo.server.service.DemoServiceImpl;
import net.apexes.wsonrpc.demo.util.SimpleJsonRpcLogger;
import net.apexes.wsonrpc.server.PathAcceptors;
import net.apexes.wsonrpc.server.http.HttpJsonRpcServer;
import net.apexes.wsonrpc.server.nano.NanoJsonRpcServer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * 
 * @author <a href="mailto:hedyn@foxmail.com">HeDYn</a>
 *
 */
public class DemoJsonRpcServer {

    private static final Logger LOG = LoggerFactory.getLogger(DemoJsonRpcServer.class);

    public static void main(String[] args) throws Exception {
        LOG.debug("...");
        HttpJsonRpcServer server = new HttpJsonRpcServer(8080, PathAcceptors.startWithPath("jsonrpc"));
        server.setJsonRpcLogger(SimpleJsonRpcLogger.DEFAULT);
//        HttpJsonRpcServer server = new HttpJsonRpcServer(8080, new JacksonImplementor());
        server.getServiceRegistry().bind(DemoService.class).toInstance(new DemoServiceImpl());
        server.start(NanoJsonRpcServer.SOCKET_READ_TIMEOUT, false);
    }

}
