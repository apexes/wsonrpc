package net.apexes.wsonrpc.demo.server;

import net.apexes.wsonrpc.core.WsonrpcConfig;
import net.apexes.wsonrpc.server.PathAcceptors;
import net.apexes.wsonrpc.server.WsonrpcServer;
import net.apexes.wsonrpc.server.support.JavaWebsocketWsonrpcServer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * 
 * @author <a href="mailto:hedyn@foxmail.com">HeDYn</a>
 *
 */
public class JwsDemoWsonrpcServerImpl extends AbstractDemoWsonrpcServer {
    
    private static final Logger LOG = LoggerFactory.getLogger(JwsDemoWsonrpcServerImpl.class);
    
    private ExecutorService execService;
    private JavaWebsocketWsonrpcServer server;
    
    @Override
    public WsonrpcServer create(int port, String path, WsonrpcConfig config) {
        execService = Executors.newCachedThreadPool();
        server = new JavaWebsocketWsonrpcServer(port, PathAcceptors.startWithPath(path), config);
        server.setPingListener((sessionId, bytes) -> LOG.info("onPing: sessionId={}, length={}", sessionId, bytes.length));
        return server.getWsonrpcServer();
    }
    
    @Override
    public void startup() {
        LOG.info("Startup...");
        execService.execute(server::start);
    }

    @Override
    public void shutdown() {
        if (server != null) {
            try {
                server.stop();
            } catch (Exception e) {
                LOG.error("", e);
            }
            server = null;
        }
        if (execService != null) {
            execService.shutdownNow();
            execService = null;
        }
    }

}
