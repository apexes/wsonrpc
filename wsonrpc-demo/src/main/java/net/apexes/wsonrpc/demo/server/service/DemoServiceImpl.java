/*
 * Copyright (C) 2016, apexes.net. All rights reserved.
 * 
 *        http://www.apexes.net
 * 
 */
package net.apexes.wsonrpc.demo.server.service;

import net.apexes.wsonrpc.demo.api.DemoService;
import net.apexes.wsonrpc.demo.api.model.Dept;
import net.apexes.wsonrpc.demo.api.model.Role;
import net.apexes.wsonrpc.demo.api.model.User;

import java.util.ArrayList;
import java.util.List;

/**
 * @author <a href="mailto:hedyn@foxmail.com">HeDYn</a>
 *
 */
public class DemoServiceImpl implements DemoService {

    public DemoServiceImpl() {
    }

    @Override
    public String echo(String text) {
        return text;
    }

    @Override
    public <T> T echo2(T vlaue) {
        return vlaue;
    }

    @Override
    public User login(String username, String password) {
        return DemoDatas.userList.get(0);
    }

    private void await(long millis) {
        try {
            Thread.sleep(millis);
        } catch (Exception e) {
            // ignore
        }
    }

    @Override
    public List<Role> getRoleList() {
        await(5000);
        return DemoDatas.roleList;
    }

    @Override
    public Dept getDept(String name) {
        return DemoDatas.deptList.get(0);
    }

    @Override
    public List<Dept> getDeptList() {
        return DemoDatas.deptList;
    }

    @Override
    public List<User> listUser(List<String> usernames) {
        List<User> userList = new ArrayList<>();
        for (String username : usernames) {
            User user = DemoDatas.userFinder.get(username);
            if (user != null) {
                userList.add(user);
            }
        }
        return userList;
    }

    @Override
    public User addUser(User user) {
        user.setRoleList(DemoDatas.roleList);
        DemoDatas.userList.add(user);
        return user;
    }

    @Override
    public void addRole(Role role) {
        DemoDatas.roleList.add(role);
    }

    @Override
    public boolean addDept(Dept detp) {
        DemoDatas.deptList.add(detp);
        return true;
    }

}
