/*
 * Copyright (C) 2016, apexes.net. All rights reserved.
 * 
 *        http://www.apexes.net
 * 
 */
package net.apexes.wsonrpc.demo.client;

import net.apexes.wsonrpc.client.JsonRpc;
import net.apexes.wsonrpc.core.RemoteInvoker;
import net.apexes.wsonrpc.demo.api.DemoService;
import net.apexes.wsonrpc.demo.util.SimpleJsonRpcLogger;
import net.apexes.wsonrpc.json.support.jackson.JacksonImplementor;

import java.util.Arrays;

/**
 * @author <a href="mailto:hedyn@foxmail.com">HeDYn</a>
 *
 */
public class DemoJsonRcpClient {
    
    public static void main(String[] args) throws Exception {
        RemoteInvoker invoker = JsonRpc.url("http://localhost:8080/jsonrpc")
                .json(JacksonImplementor.DEFAULT)
                .acceptCompress(true)
                .logger(SimpleJsonRpcLogger.DEFAULT)
                .invoker();
        DemoService demoHandler = invoker.get(DemoService.class);
        System.out.println(demoHandler.echo("Hello wsonrpc!"));
        System.out.println(demoHandler.login("admin", "admin"));
        System.out.println(demoHandler.getRoleList());
        System.out.println(demoHandler.getDept("1"));
        System.out.println(demoHandler.getDeptList());
        System.out.println(demoHandler.listUser(Arrays.asList("admin", "1001")));
    }

}
