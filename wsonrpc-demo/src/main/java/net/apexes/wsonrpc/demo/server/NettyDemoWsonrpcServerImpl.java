/*
 * Copyright (C) 2016, apexes.net. All rights reserved.
 * 
 *        http://www.apexes.net
 * 
 */
package net.apexes.wsonrpc.demo.server;

import net.apexes.wsonrpc.core.WsonrpcConfig;
import net.apexes.wsonrpc.server.WsonrpcServer;
import net.apexes.wsonrpc.server.support.NettyWsonrpcServer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * 
 * @author <a href="mailto:hedyn@foxmail.com">HeDYn</a>
 *
 */
public class NettyDemoWsonrpcServerImpl extends AbstractDemoWsonrpcServer {
    
    private static final Logger LOG = LoggerFactory.getLogger(NettyDemoWsonrpcServerImpl.class);
    
    private ExecutorService execService;
    private NettyWsonrpcServer server;
    
    @Override
    public WsonrpcServer create(int port, String path, WsonrpcConfig config) {
        execService = Executors.newCachedThreadPool();
        server = new NettyWsonrpcServer(port, path, true, config);
        server.setPingListener((sessionId, bytes) -> LOG.info("onPing: sessionId={}, length={}", sessionId, bytes.length));
        return server.getWsonrpcServer();
    }
    
    @Override
    public void startup() throws Exception {
        LOG.info("Startup...");
        execService.execute(() -> server.start());
    }

    @Override
    public void shutdown() throws Exception {
        if (server != null) {
            try {
                server.stop();
            } catch (Exception e) {
                LOG.error("", e);
            }
            server = null;
        }
        if (execService != null) {
            execService.shutdownNow();
            execService = null;
        }
    }

}
