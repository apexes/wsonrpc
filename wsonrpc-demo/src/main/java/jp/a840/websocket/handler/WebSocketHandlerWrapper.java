/*
 * The MIT License
 *
 * Copyright (c) 2011 Takahiro Hashimoto
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package jp.a840.websocket.handler;

import jp.a840.websocket.WebSocket;
import jp.a840.websocket.exception.WebSocketException;
import jp.a840.websocket.frame.Frame;

/**
 * The Class WebSocketHandlerWrapper.
 *
 * @author Takahiro Hashimoto
 */
public class WebSocketHandlerWrapper implements WebSocketHandler {

    /**
     * The handler.
     */
    private WebSocketHandler handler;

    /**
     * Instantiates a new web socket handler wrapper.
     *
     * @param handler the handler
     */
    public WebSocketHandlerWrapper(WebSocketHandler handler) {
        this.handler = handler;
    }

    @Override
    public void onOpen(WebSocket socket) {
        handler.onOpen(socket);
    }

    @Override
    public void onMessage(WebSocket socket, Frame frame) {
        handler.onMessage(socket, frame);
    }

    @Override
    public void onError(WebSocket socket, WebSocketException e) {
        handler.onError(socket, e);
    }

    @Override
    public void onClose(WebSocket socket) {
        handler.onClose(socket);
    }
}
