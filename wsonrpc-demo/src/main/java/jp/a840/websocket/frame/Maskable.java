package jp.a840.websocket.frame;

/**
 * The Class Maskable.
 *
 * @author Takahiro Hashimoto
 */
public interface Maskable {
    void mask();

    void unmask();
}
