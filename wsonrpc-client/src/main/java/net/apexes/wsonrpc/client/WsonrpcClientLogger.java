package net.apexes.wsonrpc.client;

import net.apexes.wsonrpc.core.WsonrpcLogger;

import java.net.URI;

/**
 * @author hedyn
 */
public interface WsonrpcClientLogger extends WsonrpcLogger {

    void onConnectError(URI uri, Throwable error);

    void onPingError(Throwable error);
}
