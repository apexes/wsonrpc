/*
 * Copyright (C) 2016, apexes.net. All rights reserved.
 * 
 *        http://www.apexes.net
 * 
 */
package net.apexes.wsonrpc.client;

import net.apexes.wsonrpc.core.IdGenerator;
import net.apexes.wsonrpc.core.JsonRpcEngine;
import net.apexes.wsonrpc.core.JsonRpcLogger;
import net.apexes.wsonrpc.core.Remote;
import net.apexes.wsonrpc.core.Transport;
import net.apexes.wsonrpc.core.exception.RemoteException;
import net.apexes.wsonrpc.core.exception.WsonrpcException;
import net.apexes.wsonrpc.json.JsonImplementor;
import net.apexes.wsonrpc.json.support.gson.GsonImplementor;

import java.io.BufferedInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.lang.reflect.Type;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.HashMap;
import java.util.Map;
import java.util.zip.GZIPInputStream;

/**
 * 
 * @author <a href="mailto:hedyn@foxmail.com">HeDYn</a>
 *
 */
public class HttpJsonRpcRemote implements Remote {

    private final UrlProvider urlProvider;
    private final Map<String, String> headers;
    private final JsonRpcEngine jsonRpcEngine;
    private final IdGenerator idGenerator;

    private int connectTimeout;
    private boolean acceptCompress;

    public HttpJsonRpcRemote(UrlProvider urlProvider, JsonImplementor jsonImpl, IdGenerator idGenerator) {
        this.urlProvider = urlProvider;
        this.headers = new HashMap<>();
        this.jsonRpcEngine = new JsonRpcEngine(jsonImpl == null ? GsonImplementor.DEFAULT : jsonImpl);
        this.idGenerator = idGenerator == null ? IdGenerator.DEFAULT : idGenerator;
    }

    public JsonRpcLogger getJsonRpcLogger() {
        return jsonRpcEngine.getJsonRpcLogger();
    }

    public void setJsonRpcLogger(JsonRpcLogger jsonRpcLogger) {
        jsonRpcEngine.setJsonRpcLogger(jsonRpcLogger);
    }

    public final void setHeader(String key, String value) {
        this.headers.put(key, value);
    }

    public final void setConnectTimeout(int timeout) {
        this.connectTimeout = timeout;
    }
    
    public int getConnectTimeout() {
        return connectTimeout;
    }

    public boolean isAcceptCompress() {
        return acceptCompress;
    }

    public void setAcceptCompress(boolean value) {
        acceptCompress = value;
    }

    @Override
    public void notify(String method, Object[] args) throws IOException {
        notify(method, args, null);
    }

    @Override
    public void notify(String method, Object[] args, String trace) throws IOException {
        String url = urlProvider.url();
        TransportImpl transport = new TransportImpl(url, headers, connectTimeout, 0, acceptCompress);
        try {
            jsonRpcEngine.invokeNotice(method, args, trace, transport, url);
        } finally {
            transport.close();
        }
    }

    @Override
    public <T> T request(String method, Object[] args, Type returnType, int timeout)
            throws IOException, WsonrpcException, RemoteException {
        String url = urlProvider.url();
        TransportImpl transport = new TransportImpl(url, headers, connectTimeout, timeout, acceptCompress);
        try {
            jsonRpcEngine.invoke(method, args, idGenerator.next(), transport, url);
            return jsonRpcEngine.receiveResponse(transport.readBinary(), returnType, url);
        } finally {
            transport.close();
        }
    }

    /**
     * 
     * @author <a href="mailto:hedyn@foxmail.com">HeDYn</a>
     *
     */
    private static class TransportImpl implements Transport {

        private final HttpURLConnection conn;

        TransportImpl(String url, Map<String, String> headers, int connectTimeout, int readTimeout, boolean acceptCompress)
                throws IOException {
            this.conn = (HttpURLConnection) new URL(url).openConnection();
            conn.setRequestProperty("Connection", "close");
            conn.setRequestProperty("Content-Type", "application/json;charset=utf-8");
            if (headers != null) {
                for (Map.Entry<String, String> entry : headers.entrySet()) {
                    conn.setRequestProperty(entry.getKey(), entry.getValue());
                }
            }
            if (acceptCompress) {
                conn.setRequestProperty("Accept-Encoding", "gzip");
            }
            conn.setRequestMethod("POST");
            conn.setConnectTimeout(connectTimeout);
            conn.setReadTimeout(readTimeout);
            conn.setDoOutput(true);
            conn.setDoInput(true);
            conn.connect();
        }

        @Override
        public void sendBinary(byte[] bytes) throws IOException {
            OutputStream out = conn.getOutputStream();
            out.write(bytes);
            out.flush();
        }

        public byte[] readBinary() throws IOException {
            int statusCode = conn.getResponseCode();
            if (statusCode != HttpURLConnection.HTTP_OK) {
                throw new IOException("unexpected status code returned : " + statusCode 
                        + ", message : " + conn.getResponseMessage());
            }
            String responseEncoding = conn.getHeaderField("Content-Encoding");
            responseEncoding = (responseEncoding == null ? "" : responseEncoding.trim());

            ByteArrayOutputStream out = new ByteArrayOutputStream();
            InputStream in;
            if ("gzip".equalsIgnoreCase(responseEncoding)) {
                in = new BufferedInputStream(new GZIPInputStream(conn.getInputStream()));
            } else {
                in = new BufferedInputStream(conn.getInputStream());
            }
            try {
                byte[] buf = new byte[1024];
                int len;
                while ((len = in.read(buf)) != -1) {
                    out.write(buf, 0, len);
                }
            } finally {
                in.close();
            }
            return out.toByteArray();
        }

        public void close() {
            conn.disconnect();
        }

    }

}
