package net.apexes.wsonrpc.client;

/**
 * @author hedyn
 */
public interface UrlProvider {

    String url();

    static UrlProvider fixedUrl(String url) {
        return () -> url;
    }
}
