/*
 * Copyright (c) 2018. , apexes.net. All rights reserved.
 *
 *         http://www.apexes.net
 *
 */
package net.apexes.wsonrpc.client;

import net.apexes.wsonrpc.core.WebSocketSession;
import net.apexes.wsonrpc.core.WsonrpcEngine;
import net.apexes.wsonrpc.json.JsonRpcRequest;

import java.util.concurrent.Executor;

/**
 * @author hedyn
 */
class WsonrpcClientEngine extends WsonrpcEngine {

    private final WsonrpcClientConfig config;

    WsonrpcClientEngine(WsonrpcClientConfig config) {
        super(config);
        this.config = config;
    }

    @Override
    protected void handleRequest(final WebSocketSession session, final JsonRpcRequest request) {
        Executor executor = config.getExecutor();
        if (executor != null && request != null) {
            executor.execute(() -> execReply(session, request));
        } else {
            execReply(session, request);
        }
    }

    private void execReply(WebSocketSession session, JsonRpcRequest request) {
        super.handleRequest(session, request);
    }
}
