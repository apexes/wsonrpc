package net.apexes.wsonrpc.client;

/**
 * @author hedyn
 */
public interface WsonrpcClientStatusListener {

    void onConnected(WsonrpcClient client);

    void onDisconnected(WsonrpcClient client, int code, String reason);

    void onClose(WsonrpcClient client);

}
