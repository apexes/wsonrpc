/*
 * Copyright (C) 2016, apexes.net. All rights reserved.
 * 
 *        http://www.apexes.net
 * 
 */
package net.apexes.wsonrpc.client;

import net.apexes.wsonrpc.core.RemoteInvoker;

/**
 * 
 * @author <a href="mailto:hedyn@foxmail.com">HeDYn</a>
 *
 */
public final class Wsonrpc {
    private Wsonrpc() {}

    public static WsonrpcClientBuilder config() {
        return WsonrpcClientBuilder.create();
    }

    /**
     * 创建客户端
     * @param url 连接的URL地址
     * @return 返回创建的客户端
     */
    public static WsonrpcClient client(String url) {
        return config().client(url);
    }

    /**
     * 创建远程执行器
     * @param client 客户端
     * @return 返回创建的远程执行器
     */
    public static RemoteInvoker invoker(WsonrpcClient client) {
        return RemoteInvoker.create(client.getRemote());
    }

}
