package net.apexes.wsonrpc.client;

/**
 * @author hedyn
 */
public interface WsonrpcClientMessageListener {

    void onSentMessage(WsonrpcClient client, byte[] bytes);

    void onSentPing(WsonrpcClient client, byte[] bytes);

    void onReceivePong(WsonrpcClient client, byte[] bytes);

}
