/*
 * Copyright (C) 2015, apexes.net. All rights reserved.
 * 
 *        http://www.apexes.net
 * 
 */
package net.apexes.wsonrpc.client.support;

import net.apexes.wsonrpc.client.WebsocketConnector;
import net.apexes.wsonrpc.client.WsonrpcClientEndpoint;
import net.apexes.wsonrpc.core.WebSocketSession;
import org.glassfish.tyrus.client.ClientManager;
import org.glassfish.tyrus.container.jdk.client.JdkClientContainer;

import javax.websocket.ClientEndpoint;
import javax.websocket.CloseReason;
import javax.websocket.MessageHandler;
import javax.websocket.OnClose;
import javax.websocket.OnError;
import javax.websocket.OnMessage;
import javax.websocket.OnOpen;
import javax.websocket.PongMessage;
import javax.websocket.Session;
import java.io.IOException;
import java.net.URI;
import java.nio.ByteBuffer;

/**
 * 基于Tyrus jdk client {@link org.glassfish.tyrus.container.jdk.client.JdkClientContainer}的连接
 * 
 * @author <a href=mailto:hedyn@foxmail.com>HeDYn</a>
 *
 */
public class TyrusWebsocketConnector implements WebsocketConnector {

    @Override
    public void connectToServer(WsonrpcClientEndpoint endpoint, URI uri) throws Exception {
        ClientManager mgr = ClientManager.createClient(JdkClientContainer.class.getName());
        Session session = mgr.connectToServer(new WebSocketEndpointAdapter(endpoint), uri);
        session.addMessageHandler(PongMessage.class, new PongMessageHandler(endpoint));
    }
    
    /**
     * 
     * @author <a href="mailto:hedyn@foxmail.com">HeDYn</a>
     *
     */
    @ClientEndpoint
    public static class WebSocketEndpointAdapter {
        
        private final WsonrpcClientEndpoint endpoint;
        
        public WebSocketEndpointAdapter(WsonrpcClientEndpoint endpoint) {
            this.endpoint = endpoint;
        }
        
        @OnOpen
        public void onOpen(Session session) {
            endpoint.onOpen(new WebSocketSessionAdapter(session));
        }

        @OnMessage
        public void onMessage(ByteBuffer buffer) {
            endpoint.onMessage(buffer.array());
        }

        @OnError
        public void onError(Throwable error) {
            endpoint.onError(error);
        }

        @OnClose
        public void onClose(CloseReason closeReason) {
            endpoint.onClose(closeReason.getCloseCode().getCode(), closeReason.getReasonPhrase());
        }
        
    }

    /**
     *
     * @author <a href="mailto:hedyn@foxmail.com">HeDYn</a>
     *
     */
    private static class PongMessageHandler implements MessageHandler.Whole<PongMessage> {

        private final WsonrpcClientEndpoint endpoint;

        private PongMessageHandler(WsonrpcClientEndpoint endpoint) {
            this.endpoint = endpoint;
        }

        @Override
        public void onMessage(PongMessage message) {
            endpoint.onPong(message.getApplicationData().array());
        }
    }

    /**
     *
     * @author <a href=mailto:hedyn@foxmail.com>HeDYn</a>
     *
     */
    private static class WebSocketSessionAdapter implements WebSocketSession {

        private final Session session;

        private WebSocketSessionAdapter(Session session) {
            this.session = session;
        }

        @Override
        public String getId() {
            return session.getId();
        }

        @Override
        public boolean isOpen() {
            return session.isOpen();
        }

        @Override
        public void sendBinary(byte[] bytes) throws IOException {
            session.getBasicRemote().sendBinary(ByteBuffer.wrap(bytes));
        }

        @Override
        public void ping(byte[] bytes) throws IOException {
            session.getBasicRemote().sendPing(ByteBuffer.wrap(bytes));
        }

        @Override
        public void close() throws IOException {
            session.close();
        }

    }

}
