/*
 * Copyright (C) 2016, apexes.net. All rights reserved.
 * 
 *        http://www.apexes.net
 * 
 */
package net.apexes.wsonrpc.client;

import net.apexes.wsonrpc.core.WsonrpcConfig;

import java.util.concurrent.Executor;

/**
 * 
 * @author <a href=mailto:hedyn@foxmail.com>HeDYn</a>
 *
 */
public interface WsonrpcClientConfig extends WsonrpcConfig {
    
    String getUrl();

    Executor getExecutor();

    WebsocketConnector getWebsocketConnector();

    WsonrpcClientLogger getWsonrpcClientLogger();

    /**
     * 心跳间隔时间，单位ms。为0时不发送心跳。
     */
    int getHeartbeatInterval();

    /**
     * 断开连接的心跳周期数，即当有设定值个心跳周期没有接收到数据时将自动断开连接，单位ms。为0时表示不自动断开连接。
     * 该设置项仅当发送心跳时生效。
     */
    int getHeartbeatExpireCycle();

    /**
     * 自动重连间隔时间，单位ms。
     */
    int getReconnectIntervalMin();
    int getReconnectIntervalMax();
    int getReconnectIntervalStep();

    PingProvider getPingProvider();
}
