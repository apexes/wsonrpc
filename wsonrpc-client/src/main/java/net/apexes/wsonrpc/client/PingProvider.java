package net.apexes.wsonrpc.client;

/**
 * @author <a href=mailto:hedyn@foxmail.com>HeDYn</a>
 */
public interface PingProvider {

    byte[] payload();
}
