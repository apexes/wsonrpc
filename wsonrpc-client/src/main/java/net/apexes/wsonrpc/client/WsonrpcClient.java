/*
 * Copyright (C) 2015, apexes.net. All rights reserved.
 * 
 *        http://www.apexes.net
 * 
 */
package net.apexes.wsonrpc.client;

import net.apexes.wsonrpc.core.Remote;
import net.apexes.wsonrpc.core.ServiceRegistry;

/**
 * 
 * @author <a href=mailto:hedyn@foxmail.com>HeDYn</a>
 *
 */
public interface WsonrpcClient {

    Remote getRemote();
    
    /**
     * 返回服务注册器
     * @return 返回服务注册器
     */
    ServiceRegistry getServiceRegistry();

    /**
     * 设置连接就绪后的回调
     * @param readyCallback 回调方法
     */
    void setReadyCallback(Runnable readyCallback);

    /**
     * 设置在连接断开后的回调
     * @param abortCallback 回调方法
     */
    void setAbortCallback(Runnable abortCallback);

    void setStatusListener(WsonrpcClientStatusListener listener);

    void setMessageListener(WsonrpcClientMessageListener listener);

    /**
     * 连接服务端, 连接服务端，在得到连接结果前调用此方法的线程都将阻塞
     */
    void connect() throws Exception;

    void disconnect();

    void close();

    String getSessionId();

    boolean isConnected();

    void ping(byte[] payload) throws Exception;

}
