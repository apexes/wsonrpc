package net.apexes.wsonrpc.client.support.websocket;

import java.io.DataInputStream;
import java.io.EOFException;
import java.io.IOException;
import java.net.SocketTimeoutException;

class WebSocketReceiver {

    private final WebSocketClient websocket;
    private final byte[] inputHeader = new byte[112];
    private DataInputStream input = null;
    private WebSocketEventHandler eventHandler = null;
    private MessageBuilderFactory.Builder pendingBuilder;

    private volatile boolean stop = false;

    WebSocketReceiver(WebSocketClient websocket) {
        this.websocket = websocket;
    }

    void setInput(DataInputStream input) {
        this.input = input;
    }

    void run() {
        this.eventHandler = websocket.getEventHandler();
        while (!stop) {
            try {
                int offset = 0;
                offset += read(inputHeader, offset, 1);
                boolean fin = (inputHeader[0] & 0x80) != 0;
                boolean rsv = (inputHeader[0] & 0x70) != 0;
                if (rsv) {
                    throw new WebSocketException("Invalid frame received");
                } else {
                    byte opcode = (byte) (inputHeader[0] & 0xf);
                    offset += read(inputHeader, offset, 1);
                    byte length = inputHeader[1];
                    long payloadLength = 0;
                    if (length < 126) {
                        payloadLength = length;
                    } else if (length == 126) {
                        offset += read(inputHeader, offset, 2);
                        payloadLength = ((0xff & inputHeader[2]) << 8) | (0xff & inputHeader[3]);
                    } else if (length == 127) {
                        // Does work up to MAX_VALUE of long (2^63-1) after that minus values are returned.
                        // However frames with such a high payload length are vastly unrealistic.
                        offset += read(inputHeader, offset, 8);
                        // Parse the bytes we just read
                        payloadLength = parseLong(inputHeader, offset - 8);
                    }

                    byte[] payload = new byte[(int)payloadLength];
                    offset = read(payload, 0, (int)payloadLength);
                    if (opcode == WebSocketClient.OPCODE_CLOSE) {
                        websocket.onCloseOpReceived();
                    } else if (opcode == WebSocketClient.OPCODE_PONG) {
                        websocket.onPong(payload);
                    } else if (opcode == WebSocketClient.OPCODE_TEXT ||
                            opcode == WebSocketClient.OPCODE_BINARY ||
                            opcode == WebSocketClient.OPCODE_PING ||
                            opcode == WebSocketClient.OPCODE_NONE) {
                        // It's some form of application data. Decode the payload
                        appendBytes(fin, opcode, payload);
                    } else {
                        // Unsupported opcode
                        throw new WebSocketException("Unsupported opcode: " + opcode);
                    }
                }
            } catch (SocketTimeoutException sto) {
                continue;
            } catch (EOFException e) {
                switch (websocket.getState()) {
                    case DISCONNECTING:
                    case DISCONNECTED:
                        return;
                    default:
                        handleError(new WebSocketException("IO Error", e));
                }
            } catch (IOException e) {
                handleError(new WebSocketException("IO Error", e));
            } catch (WebSocketException e) {
                handleError(e);
            }
        }
    }

    private void appendBytes(boolean fin, byte opcode, byte[] data) {
        // A ping can show up in the middle of another fragmented message
        if (opcode == WebSocketClient.OPCODE_PING) {
            if (fin) {
                handlePing(data);
            } else {
                throw new WebSocketException("PING must not fragment across frames");
            }
        } else {
            if (pendingBuilder != null && opcode != WebSocketClient.OPCODE_NONE) {
                throw new WebSocketException("Failed to continue outstanding frame");
            } else if (pendingBuilder == null && opcode == WebSocketClient.OPCODE_NONE) {
                // Trying to continue something, but there's nothing to continue
                throw new WebSocketException("Received continuing frame, but there's nothing to continue");
            } else {
                if (pendingBuilder == null) {
                    // We aren't continuing another message
                    pendingBuilder = MessageBuilderFactory.builder(opcode);
                }
                if (!pendingBuilder.appendBytes(data)) {
                    throw new WebSocketException("Failed to decode frame");
                } else if (fin) {
                    WebSocketMessage message = pendingBuilder.toMessage();
                    pendingBuilder = null;
                    // The message assembly could still fail
                    if (message == null) {
                        throw new WebSocketException("Failed to decode whole message");
                    } else {
                        eventHandler.onMessage(message);
                    }
                }
            }
        }
    }

    private void handlePing(byte[] payload) {
        if (payload.length <= 125) {
            websocket.pong(payload);
        } else {
            throw new WebSocketException("PING frame too long");
        }
    }

    private long parseLong(byte[] buffer, int offset) {
        // Copied from DataInputStream#readLong
        long v = (long) buffer[offset] << 56;
        v += ((long) buffer[offset + 1] & 0xff) << 48;
        v += ((long) buffer[offset + 2] & 0xff) << 40;
        v += ((long) buffer[offset + 3] & 0xff) << 32;
        v += (long) (buffer[offset + 4] & 255) << 24;
        v += (buffer[offset + 5] & 255) << 16;
        v += (buffer[offset + 6] & 255) << 8;
        v += (buffer[offset + 7] & 255);
        return v;
    }

    private int read(byte[] buffer, int offset, int length) throws IOException {
        input.readFully(buffer, offset, length);
        return length;
    }

    void stopit() {
        stop = true;
    }

    boolean isRunning() {
        return !stop;
    }

    private void handleError(WebSocketException e) {
        stopit();
        websocket.handleReceiverError(e);
    }
}
