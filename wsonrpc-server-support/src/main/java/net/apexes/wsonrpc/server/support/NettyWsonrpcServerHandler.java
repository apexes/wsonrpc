/*
 * Copyright (C) 2016, apexes.net. All rights reserved.
 * 
 *        http://www.apexes.net
 * 
 */
package net.apexes.wsonrpc.server.support;

import io.netty.buffer.ByteBuf;
import io.netty.buffer.Unpooled;
import io.netty.channel.Channel;
import io.netty.channel.ChannelHandler.Sharable;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.SimpleChannelInboundHandler;
import io.netty.handler.codec.http.websocketx.BinaryWebSocketFrame;
import io.netty.handler.codec.http.websocketx.PingWebSocketFrame;
import io.netty.handler.codec.http.websocketx.WebSocketFrame;
import net.apexes.wsonrpc.core.WebSocketSession;
import net.apexes.wsonrpc.core.WsonrpcConfig;
import net.apexes.wsonrpc.server.WsonrpcCloseListener;
import net.apexes.wsonrpc.server.WsonrpcMessageListener;
import net.apexes.wsonrpc.server.WsonrpcOpenListener;
import net.apexes.wsonrpc.server.WsonrpcRequestInterceptor;
import net.apexes.wsonrpc.server.WsonrpcServer;
import net.apexes.wsonrpc.server.WsonrpcServerBase;

import java.io.IOException;
import java.nio.ByteBuffer;

/**
 * 
 * @author <a href="mailto:hedyn@foxmail.com">HeDYn</a>
 *
 */
@Sharable
public class NettyWsonrpcServerHandler extends SimpleChannelInboundHandler<WebSocketFrame> {

    private static final PingWebSocketFrame PING_FRAME = new PingWebSocketFrame();
    
    protected final WsonrpcServerBase serverBase;
    
    public NettyWsonrpcServerHandler(WsonrpcConfig config) {
        this(new WsonrpcServerBase(config));
    }

    public NettyWsonrpcServerHandler(WsonrpcServerBase serverBase) {
        this.serverBase = serverBase;
    }
    
    public WsonrpcServer getWsonrpcServer() {
        return serverBase;
    }

    public void setWsonrpcOpenListener(WsonrpcOpenListener listener) {
        serverBase.setWsonrpcOpenListener(listener);
    }

    public void setWsonrpcCloseListener(WsonrpcCloseListener listener) {
        serverBase.setWsonrpcCloseListener(listener);
    }

    public void setWsonrpcMessageListener(WsonrpcMessageListener listener) {
        serverBase.setWsonrpcMessageListener(listener);
    }

    public void setWsonrpcRequestInterceptor(WsonrpcRequestInterceptor interceptor) {
        serverBase.setWsonrpcRequestInterceptor(interceptor);
    }

    @Override
    protected void channelRead0(ChannelHandlerContext ctx, WebSocketFrame msg) {
        if (msg instanceof BinaryWebSocketFrame) {
            ByteBuf buf = msg.content();
            byte[] bytes = new byte[buf.readableBytes()];
            buf.readBytes(bytes);
            serverBase.onMessage(sessionId(ctx.channel()), ByteBuffer.wrap(bytes));
        }
    }
    
    @Override
    public void handlerAdded(ChannelHandlerContext ctx) {
    }

    @Override
    public void handlerRemoved(ChannelHandlerContext ctx) {
    }
        
    @Override
    public void channelActive(ChannelHandlerContext ctx) {
        ChannelWebSocketSessionAdapter session = new ChannelWebSocketSessionAdapter(ctx);
        serverBase.onOpen(session);
    }
    
    @Override
    public void channelInactive(ChannelHandlerContext ctx) {
        Channel channel = ctx.channel();
        serverBase.onClose(sessionId(channel));
    }
    
    @Override
    public void exceptionCaught(ChannelHandlerContext ctx, Throwable cause) {
        Channel channel = ctx.channel();
        serverBase.onError(sessionId(channel), cause);
    }
    
    protected static String sessionId(Channel channel) {
        if (channel == null) {
            return null;
        }
        return Integer.toHexString(System.identityHashCode(channel));
    }

    /**
     * 
     * @author <a href="mailto:hedyn@foxmail.com">HeDYn</a>
     *
     */
    private static class ChannelWebSocketSessionAdapter implements WebSocketSession {
        
        private final ChannelHandlerContext ctx;
        private final Channel channel;
        private final String id;
        
        private ChannelWebSocketSessionAdapter(ChannelHandlerContext ctx) {
            this.ctx = ctx;
            this.channel = ctx.channel();
            this.id = sessionId(channel);
        }

        @Override
        public void sendBinary(byte[] bytes) {
            channel.writeAndFlush(new BinaryWebSocketFrame(Unpooled.wrappedBuffer(bytes)));
        }

        @Override
        public String getId() {
            return id;
        }

        @Override
        public boolean isOpen() {
            return channel.isOpen() && channel.isActive();
        }

        @Override
        public void ping(byte[] bytes) throws IOException {
            PingWebSocketFrame frame;
            if (bytes == null || bytes.length == 0) {
                frame = PING_FRAME;
            } else {
                frame = new PingWebSocketFrame(Unpooled.wrappedBuffer(bytes));
            }
            channel.writeAndFlush(frame);
        }

        @Override
        public void close() throws IOException {
            ctx.close();
        }
    }

}
