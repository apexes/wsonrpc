package net.apexes.wsonrpc.core.exception;

/**
 * @author <a href=mailto:hedyn@foxmail.com>HeDYn</a>
 */
public class WsonrpcRuntimeException extends RuntimeException {
    private static final long serialVersionUID = 1L;

    public WsonrpcRuntimeException(String message) {
        super(message);
    }

    public WsonrpcRuntimeException(String message, Throwable cause) {
        super(message, cause);
    }

    public WsonrpcRuntimeException(Throwable cause) {
        super(cause);
    }
}
