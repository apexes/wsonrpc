package net.apexes.wsonrpc.core.exception;

import net.apexes.wsonrpc.json.JsonRpcError;

/**
 * @author hedyn
 */
public class RemoteInvalidParamsException extends RemoteException {

    public static final int CODE = -32602;

    protected RemoteInvalidParamsException(String message, Throwable cause, JsonRpcError error) {
        super(message, cause, error);
    }
}
