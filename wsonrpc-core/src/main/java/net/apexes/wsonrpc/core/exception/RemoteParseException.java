package net.apexes.wsonrpc.core.exception;

import net.apexes.wsonrpc.json.JsonRpcError;

/**
 * @author hedyn
 */
public class RemoteParseException extends RemoteException {

    public static final int CODE = -32700;

    protected RemoteParseException(String message, Throwable cause, JsonRpcError error) {
        super(message, cause, error);
    }
}
