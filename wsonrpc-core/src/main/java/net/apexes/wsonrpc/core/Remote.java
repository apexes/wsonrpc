/*
 * Copyright (C) 2016, apexes.net. All rights reserved.
 * 
 *        http://www.apexes.net
 * 
 */
package net.apexes.wsonrpc.core;

import net.apexes.wsonrpc.core.exception.RemoteException;
import net.apexes.wsonrpc.core.exception.WsonrpcException;

import java.io.IOException;
import java.lang.reflect.Type;

/**
 * 
 * @author <a href="mailto:hedyn@foxmail.com">HeDYn</a>
 *
 */
public interface Remote {

    /**
     * 同步调用远程的方法。
     * 
     * @param method 服务方法
     * @param args 参数
     */
    void notify(String method, Object[] args) throws IOException, WsonrpcException;

    /**
     * 同步调用远程的方法。
     *
     * @param method 服务方法
     * @param args 参数
     * @param trace 追踪码
     */
    void notify(String method, Object[] args, String trace) throws IOException, WsonrpcException;

    /**
     * 同步调用远端方法，并返回指定类型的调用结果。
     * 
     * @param method 服务方法
     * @param args 参数
     * @param returnType 返回类型
     * @param timeout 超时时间，0表示永不超时。单位为TimeUnit.MILLISECONDS
     * @return 返回调用远程方法得到的结果
     */
    <T> T request(String method, Object[] args, Type returnType, int timeout)
            throws IOException, WsonrpcException, RemoteException;

}
