/*
 * Copyright (c) 2018, apexes.net. All rights reserved.
 *
 *         http://www.apexes.net
 *
 */
package net.apexes.wsonrpc.core;

/**
 * @author <a href=mailto:hedyn@foxmail.com>HeDYn</a>
 */
public interface TraceGenerater {

    String trace();

}
