/*
 * Copyright (C) 2015, apexes.net. All rights reserved.
 * 
 *        http://www.apexes.net
 * 
 */
package net.apexes.wsonrpc.core;

import net.apexes.wsonrpc.core.exception.WsonrpcException;

import java.io.IOException;
import java.lang.reflect.Type;
import java.util.concurrent.Future;

/**
 * 
 * @author <a href=mailto:hedyn@foxmail.com>HeDYn</a>
 *
 */
public interface WsonrpcRemote extends Remote {

    String getSessionId();
    
    boolean isConnected();

    void disconnect();
    
    void ping(byte[] payload) throws Exception;

    /**
     * 调用远程的方法，返回Future。
     */
    <T> Future<T> request(String method, Object[] args, Type returnType) throws IOException, WsonrpcException;

    /**
     * 调用远端方法。
     */
    void request(String method, Object[] args, WsonrpcCallback callback) throws IOException, WsonrpcException;

}
