/*
 * Copyright (C) 2015, apexes.net. All rights reserved.
 * 
 *        http://www.apexes.net
 * 
 */
package net.apexes.wsonrpc.core;

import net.apexes.wsonrpc.json.JsonRpcResponse;
import net.apexes.wsonrpc.util.AbstractFuture;

import java.lang.reflect.Type;

/**
 * 
 * @author <a href="mailto:hedyn@foxmail.com">HeDYn</a>
 */
public class WsonrpcFuture<T> extends AbstractFuture<T> implements WsonrpcCallback {

    private final WsonrpcEngine engine;
    private final Type returnType;

    WsonrpcFuture(WsonrpcEngine engine, Type returnType) {
        this.engine = engine;
        this.returnType = returnType;
    }

    @Override
    public void setValue(JsonRpcResponse resp) {
        try {
            T value = engine.fromResponse(resp, returnType);
            set(value);
        } catch (Throwable e) {
            setException(e);
        }
    }

    @Override
    public void setError(Throwable throwable) {
        setException(throwable);
    }
}
