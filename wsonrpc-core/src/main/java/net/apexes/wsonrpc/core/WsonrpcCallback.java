/*
 * Copyright (c) 2018, apexes.net. All rights reserved.
 *
 *         http://www.apexes.net
 *
 */
package net.apexes.wsonrpc.core;

import net.apexes.wsonrpc.json.JsonRpcResponse;

/**
 * @author <a href=mailto:hedyn@foxmail.com>HeDYn</a>
 */
public interface WsonrpcCallback {

    void setValue(JsonRpcResponse value);

    void setError(Throwable throwable);
}
