package net.apexes.wsonrpc.core.exception;

import net.apexes.wsonrpc.json.JsonRpcError;

/**
 * @author hedyn
 */
public class RemoteServerException extends RemoteException {

    public static final int MIN_CODE = -32000;
    public static final int MAX_CODE = -32099;

    protected RemoteServerException(String message, Throwable cause, JsonRpcError error) {
        super(message, cause, error);
    }
}
