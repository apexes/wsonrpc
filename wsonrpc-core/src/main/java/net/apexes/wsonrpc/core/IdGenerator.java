/*
 * Copyright (c) 2018, apexes.net. All rights reserved.
 *
 *         http://www.apexes.net
 *
 */
package net.apexes.wsonrpc.core;

import java.util.concurrent.atomic.AtomicLong;

/**
 * @author <a href=mailto:hedyn@foxmail.com>HeDYn</a>
 */
public interface IdGenerator {

    String next();

    IdGenerator DEFAULT = new IdGenerator() {
        private final AtomicLong currentId = new AtomicLong();
        @Override
        public String next() {
            return Long.toString(currentId.incrementAndGet());
        }
    };

}
