/*
 * Copyright (C) 2016, apexes.net. All rights reserved.
 * 
 *        http://www.apexes.net
 * 
 */
package net.apexes.wsonrpc.core;

import java.io.IOException;

/**
 * 
 * @author <a href="mailto:hedyn@foxmail.com">HeDYn</a>
 *
 */
public interface Transport {
    
    /**
     * 发送二进制数据
     * @param bytes 要发送的字节数组
     */
    void sendBinary(byte[] bytes) throws IOException;

}
