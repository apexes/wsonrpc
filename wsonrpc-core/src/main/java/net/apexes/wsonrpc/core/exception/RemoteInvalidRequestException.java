package net.apexes.wsonrpc.core.exception;

import net.apexes.wsonrpc.json.JsonRpcError;

/**
 * @author hedyn
 */
public class RemoteInvalidRequestException extends RemoteException {

    public static final int CODE = -32600;

    protected RemoteInvalidRequestException(String message, Throwable cause, JsonRpcError error) {
        super(message, cause, error);
    }
}
