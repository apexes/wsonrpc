package net.apexes.wsonrpc.core.exception;

import net.apexes.wsonrpc.json.JsonRpcError;

/**
 * @author hedyn
 */
public class RemoteMethodNotFoundException extends RemoteException {

    public static final int CODE = -32601;

    protected RemoteMethodNotFoundException(String message, Throwable cause, JsonRpcError error) {
        super(message, cause, error);
    }
}
