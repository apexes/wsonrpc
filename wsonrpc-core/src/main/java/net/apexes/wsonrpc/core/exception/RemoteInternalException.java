package net.apexes.wsonrpc.core.exception;

import net.apexes.wsonrpc.json.JsonRpcError;

/**
 * @author hedyn
 */
public class RemoteInternalException extends RemoteException {

    public static final int CODE = -32603;

    protected RemoteInternalException(String message, Throwable cause, JsonRpcError error) {
        super(message, cause, error);
    }
}
