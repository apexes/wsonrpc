/*
 * Copyright (C) 2016, apexes.net. All rights reserved.
 * 
 *        http://www.apexes.net
 * 
 */
package net.apexes.wsonrpc.core;

import net.apexes.wsonrpc.json.JsonImplementor;
import net.apexes.wsonrpc.json.support.gson.GsonImplementor;

/**
 * @author <a href="mailto:hedyn@foxmail.com">HeDYn</a>
 */
public class WsonrpcConfigBuilder {

    private JsonImplementor jsonImpl;
    private BinaryWrapper binaryWrapper;
    private IdGenerator idGenerator;
    private WsonrpcLogger wsonrpcLogger;

    protected WsonrpcConfigBuilder() {
    }

    protected WsonrpcConfig buildConfig() {
        if (jsonImpl == null) {
            jsonImpl = GsonImplementor.DEFAULT;
        }
        if (idGenerator == null) {
            idGenerator = IdGenerator.DEFAULT;
        }
        return new WsonrpcConfigImpl(jsonImpl, binaryWrapper, idGenerator, wsonrpcLogger);
    }

    /**
     * 设置使用的json工具，默认为 {@link GsonImplementor}
     */
    protected void setJsonImplementor(JsonImplementor jsonImpl) {
        this.jsonImpl = jsonImpl;
    }

    protected void setBinaryWrapper(BinaryWrapper binaryWrapper) {
        this.binaryWrapper = binaryWrapper;
    }

    protected void setIdGenerator(IdGenerator idGenerator) {
        this.idGenerator = idGenerator;
    }

    public void setWsonrpcLogger(WsonrpcLogger wsonrpcLogger) {
        this.wsonrpcLogger = wsonrpcLogger;
    }

    protected static class WsonrpcConfigImpl implements WsonrpcConfig {

        private final JsonImplementor jsonImpl;
        private final BinaryWrapper binaryWrapper;
        private final IdGenerator idGenerator;
        private final WsonrpcLogger wsonrpcLogger;

        protected WsonrpcConfigImpl(WsonrpcConfig config) {
            this(config.getJsonImplementor(),
                    config.getBinaryWrapper(),
                    config.getIdGenerator(),
                    config.getWsonrpcLogger());
        }

        protected WsonrpcConfigImpl(JsonImplementor jsonImpl,
                                    BinaryWrapper binaryWrapper,
                                    IdGenerator idGenerator,
                                    WsonrpcLogger wsonrpcLogger) {
            this.jsonImpl = jsonImpl;
            this.binaryWrapper = binaryWrapper;
            this.idGenerator = idGenerator;
            this.wsonrpcLogger = wsonrpcLogger;
        }

        @Override
        public JsonImplementor getJsonImplementor() {
            return jsonImpl;
        }

        @Override
        public BinaryWrapper getBinaryWrapper() {
            return binaryWrapper;
        }

        @Override
        public IdGenerator getIdGenerator() {
            return idGenerator;
        }

        @Override
        public WsonrpcLogger getWsonrpcLogger() {
            return wsonrpcLogger;
        }
    }

}
