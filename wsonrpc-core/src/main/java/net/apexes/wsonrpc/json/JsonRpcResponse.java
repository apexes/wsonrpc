/*
 * Copyright (c) 2018, apexes.net. All rights reserved.
 *
 *         http://www.apexes.net
 *
 */
package net.apexes.wsonrpc.json;

import net.apexes.wsonrpc.core.exception.JsonException;

import java.lang.reflect.Type;

/**
 * @author <a href=mailto:hedyn@foxmail.com>HeDYn</a>
 */
public interface JsonRpcResponse extends JsonRpcMessage {

    Object getResult();

    Object getResultValue(Type type);

    String getResultAsJson() throws JsonException;

    JsonRpcError getError();

}
