/*
 * Copyright (C) 2016, apexes.net. All rights reserved.
 * 
 *        http://www.apexes.net
 * 
 */
package net.apexes.wsonrpc.json;

import net.apexes.wsonrpc.core.exception.JsonException;

/**
 * 
 * @author <a href="mailto:hedyn@foxmail.com">HeDYn</a>
 *
 */
public interface JsonImplementor {

    JsonRpcMessage fromJson(String json) throws JsonException;

    JsonRpcRequest createRequest(String id, String method, Object[] params);

    JsonRpcRequest createNotice(String method, Object[] params, String trace);

    JsonRpcResponse createResponse(String id, Object result);

    JsonRpcResponse createResponse(JsonRpcError error);

    JsonRpcResponse createResponse(String id, JsonRpcError error);

}
