/*
 * Copyright (c) 2018, apexes.net. All rights reserved.
 *
 *         http://www.apexes.net
 *
 */
package net.apexes.wsonrpc.json;

import java.lang.reflect.Type;

/**
 * @author <a href=mailto:hedyn@foxmail.com>HeDYn</a>
 */
public interface JsonRpcRequest extends JsonRpcMessage {

    String getTrace();

    String getMethod();

    int getParamCount();

    Object getParams();

    Object[] getParams(Type[] types);

    String getParamsAsJson();

    Object getParam(int index);

    String getParamAsJson(int index);

}
