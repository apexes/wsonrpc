/*
 * Copyright (c) 2018, apexes.net. All rights reserved.
 *
 *         http://www.apexes.net
 *
 */
package net.apexes.wsonrpc.json;

/**
 * @author <a href=mailto:hedyn@foxmail.com>HeDYn</a>
 */
public interface JsonRpcMessage {

    String getId();

    String toJson();

    boolean isRequest();

    boolean isResponse();

}
