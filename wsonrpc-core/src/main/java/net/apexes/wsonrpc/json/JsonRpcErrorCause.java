/*
 * Copyright (c) 2018, apexes.net. All rights reserved.
 *
 *         http://www.apexes.net
 *
 */
package net.apexes.wsonrpc.json;

import java.util.List;

/**
 * @author <a href=mailto:hedyn@foxmail.com>HeDYn</a>
 */
public class JsonRpcErrorCause {

    private String errorClass;
    private String errorMessage;
    private List<JsonRpcErrorCauseStackTrace> stackTraces;
    private JsonRpcErrorCause cause;

    public String getErrorClass() {
        return errorClass;
    }

    public void setErrorClass(String errorClass) {
        this.errorClass = errorClass;
    }

    public String getErrorMessage() {
        return errorMessage;
    }

    public void setErrorMessage(String errorMessage) {
        this.errorMessage = errorMessage;
    }

    public List<JsonRpcErrorCauseStackTrace> getStackTraces() {
        return stackTraces;
    }

    public void setStackTraces(List<JsonRpcErrorCauseStackTrace> stackTraces) {
        this.stackTraces = stackTraces;
    }

    public JsonRpcErrorCause getCause() {
        return cause;
    }

    public void setCause(JsonRpcErrorCause cause) {
        this.cause = cause;
    }
}
