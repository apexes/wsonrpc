/*
 * Copyright (c) 2018, apexes.net. All rights reserved.
 *
 *         http://www.apexes.net
 *
 */
package net.apexes.wsonrpc.json;

/**
 * 
 * @author <a href="mailto:hedyn@foxmail.com">HeDYn</a>
 *
 */
public class JsonRpcError {
    
    private final Integer code;
    private final String message;
    private final JsonRpcErrorCause data;

    public JsonRpcError(Integer code, String message, JsonRpcErrorCause data) {
        this.code = code;
        this.message = message;
        this.data = data;
    }

    public Integer getCode() {
        return code;
    }

    public String getMessage() {
        return message;
    }

    public JsonRpcErrorCause getData() {
        return data;
    }

}
