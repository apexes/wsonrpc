# wsonrpc

#### 介绍
基于WebSocket的JsonRpc，实现Server端与Client之间的双向互访，可应用在android项目上。


#### 安装教程

客户端：
```
<dependency>
  <groupId>net.apexes.wsonrpc</groupId>
  <artifactId>wsonrpc-client</artifactId>
  <version>${version.wsonrpc}</version>
</dependency>
```

服务端：
```
<dependency>
  <groupId>net.apexes.wsonrpc</groupId>
  <artifactId>wsonrpc-server</artifactId>
  <version>${version.wsonrpc}</version>
</dependency>
<dependency>
  <groupId>net.apexes.wsonrpc</groupId>
  <artifactId>wsonrpc-server-support</artifactId>
  <version>${version.wsonrpc}</version>
</dependency>
```

#### 使用说明

Server 端：

```
@MaxSessions(10000)
@ServerEndpoint("/wsonrpc")
public class Jsr356DemoWsonrpcServer {

    private final WsonrpcServerBase serverBase;

    public Jsr356DemoWsonrpcServer() {
        serverBase = new WsonrpcServerBase(WsonrpcConfigBuilder.create()
                .json(new JacksonImplementor())
                .wrapper(new GZIPBinaryWrapper())
                .logger(new SimpleWsonrpcLogger())
                .build());
        serverBase.getServiceRegistry()
                .bind(DemoService.class).toInstance(new DemoServiceImpl())
                .bind(RegisterService.class).toInstance(new RegisterServiceImpl());
    }

    @OnOpen
    public void onOpen(Session session) {
        serverBase.onOpen(WebSockets.createSession(session));
    }

    @OnClose
    public void onClose(Session session, CloseReason closeReason) {
        serverBase.onClose(session.getId());
    }

    @OnMessage
    public void onMessage(final Session session, final ByteBuffer buffer) {
        serverBase.onMessage(session.getId(), buffer);
    }
}
```


Client 使用示例：

```
String url = "ws://localhost:8080/wsonrpc";
WsonrpcClient client = Wsonrpc.config()
        .wrapper(new net.apexes.wsonrpc.core.GZIPBinaryWrapper())
        .logger(new SimpleWsonrpcLogger())
        .heartbeatSeconds(6, 1)
        .reconnectSeconds(5, 30, 2)
        .pingProvider(() -> String.valueOf(System.currentTimeMillis()).getBytes())
        .client(url);
client.getServiceRegistry().bind(PushService.class).toInstance(new PushServiceImpl(client));
client.setReadyCallback(() -> {
    Wsonrpc.invoker(client)
            .get(RegisterService.class)
            .register(clientId);
});
```

