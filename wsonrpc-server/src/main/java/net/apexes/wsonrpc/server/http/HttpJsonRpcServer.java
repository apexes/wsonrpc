package net.apexes.wsonrpc.server.http;

import net.apexes.wsonrpc.core.JsonRpcLogger;
import net.apexes.wsonrpc.core.ServiceRegistry;
import net.apexes.wsonrpc.json.JsonImplementor;
import net.apexes.wsonrpc.server.PathAcceptor;
import net.apexes.wsonrpc.server.nano.NanoJsonRpcServer;

import java.io.IOException;

/**
 * @author hedyn
 */
public class HttpJsonRpcServer {

    private final NanoJsonRpcServer nanoJsonRpcServer;

    public HttpJsonRpcServer(int port) {
        this.nanoJsonRpcServer = new NanoJsonRpcServer(port);
    }

    public HttpJsonRpcServer(int port, PathAcceptor pathAcceptor) {
        this.nanoJsonRpcServer = new NanoJsonRpcServer(port, pathAcceptor);
    }

    public HttpJsonRpcServer(int port, PathAcceptor pathAcceptor, JsonImplementor jsonImpl) {
        this.nanoJsonRpcServer = new NanoJsonRpcServer(port, pathAcceptor, jsonImpl);
    }

    public ServiceRegistry getServiceRegistry() {
        return nanoJsonRpcServer.getServiceRegistry();
    }

    public JsonRpcLogger getJsonRpcLogger() {
        return nanoJsonRpcServer.getJsonRpcLogger();
    }

    public void setJsonRpcLogger(JsonRpcLogger jsonRpcLogger) {
        nanoJsonRpcServer.setJsonRpcLogger(jsonRpcLogger);
    }

    public HttpRequestValidator getRequestValidator() {
        return nanoJsonRpcServer.getRequestValidator();
    }

    public void setRequestValidator(HttpRequestValidator requestValidator) {
        nanoJsonRpcServer.setRequestValidator(requestValidator);
    }

    public void start() throws IOException {
        nanoJsonRpcServer.start();
    }

    public void start(int timeout) throws IOException {
        nanoJsonRpcServer.start(timeout, true);
    }

    public void start(int timeout, boolean daemon) throws IOException {
        nanoJsonRpcServer.start(timeout, daemon);
    }

    public void stop() {
        nanoJsonRpcServer.stop();
    }
}
