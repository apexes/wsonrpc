/*
 * Copyright (c) 2018, apexes.net. All rights reserved.
 *
 *         http://www.apexes.net
 *
 */
package net.apexes.wsonrpc.server;

/**
 * @author <a href=mailto:hedyn@foxmail.com>HeDYn</a>
 */
public final class PathAcceptors {

    /**
     * 允许接受以root开头的路径连接
     * @param root
     * @return
     */
    public static PathAcceptor startWithPath(String root) {
        final String rootPath = path(root);
        return path -> path.startsWith(rootPath);
    }

    public static PathAcceptor equalsPath(String ref) {
        final String refPath = path(ref);
        return path -> path.equals(refPath);
    }

    public static PathAcceptor anyPath() {
        return path -> true;
    }

    public static PathAcceptor rootPath() {
        return equalsPath("/");
    }

    private static String path(String ref) {
        final String path;
        if (ref.startsWith("/")) {
            path = ref;
        } else {
            path = "/" + ref;
        }
        return path;
    }

}
