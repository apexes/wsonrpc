package net.apexes.wsonrpc.server.http;

import java.util.function.Function;

/**
 * @author hedyn
 */
public interface HttpRequestValidator {

    boolean validate(Function<String, String> headerGetter, byte[] bodyBytes);

}
