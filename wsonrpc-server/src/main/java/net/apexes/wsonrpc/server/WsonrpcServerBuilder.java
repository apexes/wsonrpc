package net.apexes.wsonrpc.server;

import net.apexes.wsonrpc.core.BinaryWrapper;
import net.apexes.wsonrpc.core.IdGenerator;
import net.apexes.wsonrpc.core.WsonrpcConfig;
import net.apexes.wsonrpc.core.WsonrpcConfigBuilder;
import net.apexes.wsonrpc.core.WsonrpcLogger;
import net.apexes.wsonrpc.json.JsonImplementor;

/**
 * @author hedyn
 */
public class WsonrpcServerBuilder extends WsonrpcConfigBuilder {

    public static WsonrpcConfig defaultConfig() {
        return create().build();
    }

    public static WsonrpcServerBuilder create() {
        return new WsonrpcServerBuilder();
    }

    private WsonrpcServerBuilder() {
    }

    public WsonrpcServerBuilder json(JsonImplementor jsonImpl) {
        super.setJsonImplementor(jsonImpl);
        return this;
    }

    public WsonrpcServerBuilder wrapper(BinaryWrapper binaryWrapper) {
        super.setBinaryWrapper(binaryWrapper);
        return this;
    }

    public WsonrpcServerBuilder idGenerater(IdGenerator idGenerater) {
        super.setIdGenerator(idGenerater);
        return this;
    }

    public WsonrpcServerBuilder logger(WsonrpcLogger wsonrpcLogger) {
        super.setWsonrpcLogger(wsonrpcLogger);
        return this;
    }

    public WsonrpcConfig build() {
        return buildConfig();
    }

}
