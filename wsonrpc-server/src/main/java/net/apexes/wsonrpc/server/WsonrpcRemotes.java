/*
 * Copyright (C) 2015, apexes.net. All rights reserved.
 * 
 *        http://www.apexes.net
 * 
 */
package net.apexes.wsonrpc.server;

import net.apexes.wsonrpc.core.WebSocketSession;
import net.apexes.wsonrpc.core.WsonrpcRemote;

import java.util.Collection;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;


/**
 * 
 * @author <a href=mailto:hedyn@foxmail.com>HeDYn</a>
 *
 */
public final class WsonrpcRemotes {
    
    private WsonrpcRemotes() {}

    private static final Map<String, RemoteWsonrpcEndpoint> REMOTES = new ConcurrentHashMap<>();

    static void addRemote(WebSocketSession session, RemoteWsonrpcEndpoint endpoint) {
        REMOTES.put(session.getId(), endpoint);
    }
    
    static void removeRemote(String sessionId) {
        REMOTES.remove(sessionId);
    }

    static WebSocketSession getSession(String sessionId) {
        if (sessionId == null) {
            return null;
        }
        RemoteWsonrpcEndpoint endpoint = REMOTES.get(sessionId);
        if (endpoint == null) {
            return null;
        }
        return endpoint.getSession();
    }
    
    /**
     * 返回指定ID的客户端连接
     * 
     * @param sessionId
     * @return
     */
    public static WsonrpcRemote getRemote(String sessionId) {
        if (sessionId == null) {
            return null;
        }
        return REMOTES.get(sessionId);
    }

    /**
     * 返回所有客户端连接
     * 
     * @return
     */
    public static Collection<? extends WsonrpcRemote> getRemotes() {
        return REMOTES.values();
    }
    
    /**
     * 返回当前线程的客户端连接
     * 
     * @return
     */
    public static WsonrpcRemote getRemote() {
        WebSocketSession session = WsonrpcSessions.get();
        if (session != null) {
            return getRemote(session.getId());
        }
        return null;
    }
}
