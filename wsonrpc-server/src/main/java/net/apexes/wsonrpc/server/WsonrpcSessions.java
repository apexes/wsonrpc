/*
 * Copyright (C) 2015, apexes.net. All rights reserved.
 * 
 *        http://www.apexes.net
 * 
 */
package net.apexes.wsonrpc.server;

import net.apexes.wsonrpc.core.WebSocketSession;

/**
 * 
 * @author <a href=mailto:hedyn@foxmail.com>HeDYn</a>
 *
 */
public final class WsonrpcSessions {

    private WsonrpcSessions() {}

    private static final ThreadLocal<WebSocketSession> SESSIONS = new ThreadLocal<>();

    static void begin(WebSocketSession session) {
        SESSIONS.set(session);
    }

    static void end() {
        SESSIONS.remove();
    }

    public static WebSocketSession get() {
        return SESSIONS.get();
    }
}
