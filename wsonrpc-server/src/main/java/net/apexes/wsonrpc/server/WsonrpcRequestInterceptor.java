/*
 * Copyright (c) 2018. , apexes.net. All rights reserved.
 *
 *         http://www.apexes.net
 *
 */
package net.apexes.wsonrpc.server;

import net.apexes.wsonrpc.core.WebSocketSession;
import net.apexes.wsonrpc.json.JsonRpcError;
import net.apexes.wsonrpc.json.JsonRpcRequest;

import java.lang.reflect.Type;

/**
 * Wsonrpc请求拦截器
 *
 * @author <a href="mailto:hedyn@foxmail.com">HeDYn</a>
 */
public interface WsonrpcRequestInterceptor {

    /**
     * 可以调用{@link Context#executeReply()}方法触发执行并回复；
     * 或者调用{@link Context#replyValue(Object)}进行回复
     *
     * @param context 上下文
     */
    void handle(Context context);

    /**
     *
     * @author <a href="mailto:hedyn@foxmail.com">HeDYn</a>
     */
    interface Context {

        /**
         * 返回当前会话
         * @return 返回当前会话
         */
        WebSocketSession getSession();

        /**
         * 获取请求对象
         * @return 返回请求对象
         */
        JsonRpcRequest getRequest();

        /**
         * 返回请求方法
         * @return 返回请求方法
         * @deprecated 使用 {@link JsonRpcRequest#getMethod()}
         */
        @Deprecated
        String getServiceMethod();

        /**
         * 返回请求ID
         * @return 返回请求ID
         * @deprecated 使用 {@link JsonRpcRequest#getId()}
         */
        @Deprecated
        String getRequestId();

        /**
         * 返回参数
         * @return 返回参数
         * @deprecated 使用 {@link JsonRpcRequest#getParams()}
         */
        @Deprecated
        Object getParams();

        /**
         * 返回适配指定类型的参数数组
         * @param types 类型
         * @return 返回适配指定类型的参数数组
         * @deprecated 使用 {@link JsonRpcRequest#getParams(Type[])}
         */
        @Deprecated
        Object[] getParams(Type[] types);

        /**
         * 返回参数的json
         * @return 返回参数的json
         * @deprecated 使用 {@link JsonRpcRequest#getParamsAsJson()}
         */
        @Deprecated
        String getParamsAsJson();

        /**
         * 如果是通知就返回true
         * @return 如果是通知就返回true
         */
        boolean isNotification();

        /**
         * 触发执行并回复
         */
        void executeReply();

        /**
         * 回复
         * @param result 回复的内容
         */
        void replyValue(Object result);

        /**
         * 回复异常
         * @param t 异常
         */
        void replyThrowable(Throwable t);

        /**
         * 回复异常
         * @param error 异常
         */
        void replyError(JsonRpcError error);
    }

}
