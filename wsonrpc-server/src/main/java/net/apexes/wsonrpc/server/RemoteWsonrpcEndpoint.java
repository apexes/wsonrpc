/*
 * Copyright (c) 2018, apexes.net. All rights reserved.
 *
 *         http://www.apexes.net
 *
 */
package net.apexes.wsonrpc.server;

import net.apexes.wsonrpc.core.WebSocketSession;
import net.apexes.wsonrpc.core.WsonrpcConfig;
import net.apexes.wsonrpc.core.WsonrpcEndpoint;
import net.apexes.wsonrpc.core.WsonrpcEngine;

/**
 * @author <a href=mailto:hedyn@foxmail.com>HeDYn</a>
 */
public class RemoteWsonrpcEndpoint extends WsonrpcEndpoint {

    protected RemoteWsonrpcEndpoint(WebSocketSession session, WsonrpcServerEngine engine) {
        super(engine);
        openSession(session);
    }

    protected RemoteWsonrpcEndpoint(WebSocketSession session, WsonrpcEngine engine) {
        super(engine);
        openSession(session);
    }

    @Override
    protected WebSocketSession getSession() {
        return super.getSession();
    }
}
