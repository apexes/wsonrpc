package net.apexes.wsonrpc.server;

import net.apexes.wsonrpc.core.WebSocketSession;

/**
 * @author hedyn
 */
public interface WsonrpcOpenListener {
    void onOpened(WebSocketSession session);
}
