/*
 * Copyright (C) 2016, apexes.net. All rights reserved.
 * 
 *        http://www.apexes.net
 * 
 */
package net.apexes.wsonrpc.server;

import net.apexes.wsonrpc.core.ServiceRegistry;

/**
 * 
 * @author <a href="mailto:hedyn@foxmail.com">HeDYn</a>
 *
 */
public interface WsonrpcServer {
    
    ServiceRegistry getServiceRegistry();

    void setWsonrpcOpenListener(WsonrpcOpenListener listener);

    void setWsonrpcCloseListener(WsonrpcCloseListener listener);

    void setWsonrpcMessageListener(WsonrpcMessageListener listener);

    void setWsonrpcRequestInterceptor(WsonrpcRequestInterceptor interceptor);
    
}
