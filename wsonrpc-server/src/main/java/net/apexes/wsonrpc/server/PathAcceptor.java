/*
 * Copyright (c) 2018, apexes.net. All rights reserved.
 *
 *         http://www.apexes.net
 *
 */
package net.apexes.wsonrpc.server;

/**
 * @author <a href=mailto:hedyn@foxmail.com>HeDYn</a>
 */
public interface PathAcceptor {

    /**
     * 返回true表示此路径接受连接
     * @param path
     * @return
     */
    boolean accept(String path);
}
