package net.apexes.wsonrpc.server;

/**
 * @author hedyn
 */
public interface WsonrpcMessageListener {
    void onMessage(String sessionId, byte[] bytes);
}
