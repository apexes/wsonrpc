package net.apexes.wsonrpc.server;

/**
 * @author hedyn
 */
public interface WsonrpcCloseListener {
    void onClosed(String sessionId);
}
